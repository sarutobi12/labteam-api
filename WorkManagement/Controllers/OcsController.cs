﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Data.ViewModel.OC;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using AutoMapper;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OcsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IOCService _ocService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;
        private readonly IMapper _mapper;
        public OcsController(DataContext context, IMapper mapper, IOCService ocService, IWebHostEnvironment environment, IConfiguration configuaration)
        {
            _ocService = ocService;
            _context = context;
            _environment = environment;
            _configuaration = configuaration;
            _mapper = mapper;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult> GetListTree()
        {
            return Ok(await _ocService.GetListTree());
        }

        // GET: api/Projects/5
        [HttpPost]
        public async Task<ActionResult> AddOrUpdate(OC oC)
        {
            return Ok( await _ocService.AddOrUpdate(oC));
        }

        // PUT: api/Projects/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<IActionResult> Rename([FromBody]TreeViewRename oC)
        {
            return Ok(await _ocService.Rename(oC));
        }

        [HttpPost]
        public async Task<ActionResult> Rename2([FromForm]TreeViewRename entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var name = Request.Form["UploadedFileName"];
                var id = Request.Form["UploadedFileID"];
                var path = Request.Form["UploadedFilePath"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\video\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\video\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\video\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.key = int.Parse(id);
                        var item = await _context.OCs.FindAsync(entity.key);
                        item.Name = name;
                        item.Path = path;
                        item.URL = _configuaration["AppSettings:applicationUrl"] + $"/video/{file.FileName}";
                        //return "\\image\\" + file.FileName;
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    entity.key = int.Parse(id);
                    var item = await _context.OCs.FindAsync(entity.key);
                    item.Name = name;
                    item.Path = path;
                    await _context.SaveChangesAsync();
                }
                //_context.Add(entity);
                //await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }

        [HttpPost]
        public async Task<IActionResult> CreateOC([FromBody]CreateOCViewModel oC)
        {
            return Ok(await _ocService.CreateOC(oC));
        }

        [HttpPost]
        public async Task<ActionResult> CreateOC2([FromForm]CreateOCViewModel entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var name = Request.Form["UploadedFileName"];
                var parentid = Request.Form["UploadedFileParentID"];
                var path = Request.Form["UploadedFilePath"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\video\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\video\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\video\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();

                        var item = _mapper.Map<Data.Models.OC>(entity);
                        item.Level = 1;
                        item.Name = name;
                        item.ParentID = int.Parse(parentid);
                        item.URL = _configuaration["AppSettings:applicationUrl"] + $"/video/{file.FileName}";
                        item.Path = path;
                        //return "\\image\\" + file.FileName;
                        await _context.OCs.AddAsync(item);
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    var item = _mapper.Map<Data.Models.OC>(entity);
                    item.Level = 1;
                    item.Name = name;
                    item.Path = path;
                    item.ParentID = int.Parse(parentid);
                    await _context.OCs.AddAsync(item);
                    await _context.SaveChangesAsync();
                }
                //_context.Add(entity);
                //await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSubOC([FromBody]CreateOCViewModel oC)
        {
            return Ok(await _ocService.CreateSubOC(oC));
        }

        [HttpPost]
        public async Task<ActionResult> CreateSubOC2([FromForm]CreateOCViewModel entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var name = Request.Form["UploadedFileName"];
                var level = Request.Form["UploadedFileLevel"];
                var parentid = Request.Form["UploadedFileParentID"];
                var path = Request.Form["UploadedFilePath"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\video\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\video\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\video\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();

                        var item = _mapper.Map<Data.Models.OC>(entity);
                        item.ParentID = int.Parse(parentid);
                        //Level cha tang len 1 va gan parentid cho subtask
                        var taskParent = _context.OCs.Find(item.ParentID);
                        item.Name = name;
                        item.Level = int.Parse(level);
                        item.ParentID = int.Parse(parentid);
                        item.Path = path;
                        item.URL = _configuaration["AppSettings:applicationUrl"] + $"/video/{file.FileName}";
                        await _context.OCs.AddAsync(item);
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    var item = _mapper.Map<Data.Models.OC>(entity);
                    item.ParentID = int.Parse(parentid);
                    //Level cha tang len 1 va gan parentid cho subtask
                    var taskParent = _context.OCs.Find(item.ParentID);
                    item.Name = name;
                    //item.Level = int.Parse(level);
                    item.Level = float.Parse(level);
                    item.ParentID = int.Parse(parentid);
                    item.Path = path;
                    await _context.OCs.AddAsync(item);
                    await _context.SaveChangesAsync();
                }
                //_context.Add(entity);
                //await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _ocService.Delete(id));
        }
    }
}
