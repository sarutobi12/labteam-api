﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IArticleService _articleService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public ArticlesController(DataContext context, IArticleService articleService, IWebHostEnvironment environment, IConfiguration configuaration)
        {
            _context = context;
            _articleService = articleService;
            _environment = environment;
            _configuaration = configuaration;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _articleService.GetAll());
        }


        [HttpPost]
        public async Task<ActionResult<Article>> Create(Article project)
        {
            return Ok(await _articleService.Create(project));
        }

        [HttpPost]
        public async Task<ActionResult<Article>> Created2([FromForm]Article entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["image"];
                //var name = Request.Form["UploadedFileName"];
                //var URL = Request.Form["UploadedFileURL"];
                //var WorkBy = Request.Form["UploadedFileWorkBy"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\image\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\image\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\image\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        //entity.Name = name;
                        //entity.URL = URL;
                        //entity.WorkBy = WorkBy;
                        //entity.CreateTime = DateTime.Now;
                        entity.URL = _configuaration["AppSettings:applicationUrl"] + $"/image/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }

                _context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }

        [HttpPost]
        public async Task<IActionResult> Update(Article project)
        {
            return Ok(await _articleService.Update(project));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Project>> DeleteArticle(int id)
        {
            return Ok(await _articleService.Delete(id));
        }
        [HttpGet("{id}")]
        public async Task<ActionResult> GetByID(int id)
        {
            var project = await _articleService.GetByID(id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }
    }
}