﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Data;
using Data.Models;
using Service.Interface;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace WorkManagement.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IProjectService _projectService;
        public readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuaration;

        public ProjectsController(DataContext context, IProjectService projectService, IWebHostEnvironment environment, IConfiguration configuration)
        {
            _configuaration = configuration;
            _context = context;
            _projectService = projectService;
            _environment = environment;

        }

        // GET: api/Projects
        [HttpGet("{keyword}/{page}/{pageSize}")]
        public async Task<ActionResult> GetAllPaging(string keyword, int page, int pageSize)
        {
            return Ok(await _projectService.GetAllPaging(keyword, page, pageSize));
        }
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _projectService.GetAll());
        }
        [HttpGet]
        public async Task<ActionResult> GetListProject()
        {
            return Ok(await _projectService.GetListProject());
        }
        // GET: api/Projects/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult> GetByID(int id)
        //{
        //    var project = await _projectService.GetByID(id);

        //    if (project == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(project);
        //}

        // PUT: api/Projects/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<IActionResult> Update([FromForm]Project entity)
        {

            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var name = Request.Form["UploadedFilenameEdit"];
                var URL = Request.Form["UploadedFileURLEdit"];
                var WorkBy = Request.Form["UploadedFileWorkByEdit"];
                var ID = Request.Form["UploadedFileID"];
                //var item = await _context.Projects.FindAsync(entity.ID);
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\image\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\image\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\image\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.ID = int.Parse(ID);
                        var item = await _context.Projects.FindAsync(entity.ID);
                        item.Name = name;
                        item.URL = URL;
                        item.WorkBy = WorkBy;
                        item.Image = _configuaration["AppSettings:applicationUrl"] + $"/image/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }
                else
                {
                    entity.ID = int.Parse(ID);
                    var item = await _context.Projects.FindAsync(entity.ID);
                    item.Name = name;
                    item.URL = URL;
                    item.WorkBy = WorkBy;
                }

                //_context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
            //try
            //{
            //    await _context.SaveChangesAsync();
            //    return Ok(entity);
            //}
            //catch (Exception)
            //{
            //    var errors = ModelState.Values.SelectMany(v => v.Errors);
            //}
            //return Ok(entity);
        }

        // POST: api/Projects
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Project>> Create(Project project)
        {
            return Ok(await _projectService.Create(project));
        }



        [HttpPost]
        public async Task<ActionResult<Project>> Created2([FromForm]Project entity)
        {
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files["UploadedFile"];
                var name = Request.Form["UploadedFileName"];
                var URL = Request.Form["UploadedFileURL"];
                var WorkBy = Request.Form["UploadedFileWorkBy"];
                if (file != null)
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\image\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\image\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\image\\" + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        entity.Name = name;
                        entity.URL = URL;
                        entity.WorkBy = WorkBy;
                        //entity.CreateTime = DateTime.Now;
                        entity.Image = _configuaration["AppSettings:applicationUrl"] + $"/image/{file.FileName}";
                        //return "\\image\\" + file.FileName;

                    }
                }

                _context.Add(entity);
                await _context.SaveChangesAsync();
                return Ok(entity);

            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }
            return Ok(entity);
        }

        
        // DELETE: api/Projects/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Project>> DeleteProject(int id)
        {
            return Ok(await _projectService.Delete(id));
        }


        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetFile(string id)
        //{
        //    var imageFileStream = System.IO.File.OpenRead("// image path");
        //    return File(imageFileStream, "image/jpeg");
        //}

        [HttpGet("id")]
        public IActionResult GetImageByID(int id)
        {
            Byte[] b = System.IO.File.ReadAllBytes(@"image/file.png");   // You can use your own method over here.         
            return File(b, "image/jpeg");
        }

    }
}
