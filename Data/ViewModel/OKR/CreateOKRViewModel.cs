﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ViewModel.OKR
{
   public class CreateOKRViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int Parentid { get; set; }

    }
}
