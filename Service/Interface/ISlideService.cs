﻿using Data.Models;
using Data.ViewModel.Project;
using Data.ViewModel.Slide;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface ISlideService
    {
        Task<bool> Create(Slide entity);
        Task<bool> Update(Slide entity);
        Task<bool> Delete(int id);
        Task<Slide> GetByID(int id);
        Task<List<Slide>> GetAll();
        Task<PagedList<Slide>> GetAllPaging(string keyword,int page, int pageSize);
        Task<List<SlideViewModel>> GetListSlide();
    }
}
