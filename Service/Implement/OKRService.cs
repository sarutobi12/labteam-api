﻿using AutoMapper;
using Data;
using Data.Models;
using Data.ViewModel.OKR;
using Microsoft.EntityFrameworkCore;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class OKRService : IOkrService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public OKRService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<bool> AddOrUpdate(OC entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<TreeView>> GetListTree()
        {
            var listLevels = await _context.OKRs.OrderBy(x => x.Level).ToListAsync();
            var levels = new List<TreeView>();
            foreach (var item in listLevels)
            {
                var levelItem = new TreeView();
                levelItem.key = item.ID;
                levelItem.title = item.Name;
                levelItem.levelnumber = item.Level;
                levelItem.parentid = item.Parentid;
                levels.Add(levelItem);
            }

            List<TreeView> hierarchy = new List<TreeView>();

            hierarchy = levels.Where(c => c.parentid == 0)
                            .Select(c => new TreeView()
                            {
                                key = c.key,
                                title = c.title,
                                code = c.code,
                                levelnumber = c.levelnumber,
                                parentid = c.parentid,
                                link = c.link,
                                linkpath = c.linkpath,
                                children = GetChildren(levels, c.key)
                            })
                            .ToList();


            HieararchyWalk(hierarchy);

            return hierarchy;
        }
        private void HieararchyWalk(List<TreeView> hierarchy)
        {
            if (hierarchy != null)
            {
                foreach (var item in hierarchy)
                {
                    //Console.WriteLine(string.Format("{0} {1}", item.Id, item.Text));
                    HieararchyWalk(item.children);
                }
            }
        }
        public List<TreeView> GetChildren(List<TreeView> levels, int parentid)
        {
            return levels
                    .Where(c => c.parentid == parentid)
                    .Select(c => new TreeView()
                    {
                        key = c.key,
                        title = c.title,
                        code = c.code,
                        levelnumber = c.levelnumber,
                        parentid = c.parentid,
                        link = c.link,
                        linkpath = c.linkpath,
                        children = GetChildren(levels, c.key)
                    })
                    .ToList();
        }

        public string GetNode(string code)
        {
            throw new NotImplementedException();

        }

        public string GetNode(int id)
        {
            var list = new List<OC>();
            list = _context.OCs.ToList();
            var list2 = new List<OC>();
            list2.Add(list.FirstOrDefault(x => x.ID == id));
            var parentID = list.FirstOrDefault(x => x.ID == id).ParentID;
            foreach (var item in list)
            {
                if (parentID == 0)
                    break;
                if (parentID != 0)
                {
                    //add vao list1
                    list2.Add(list.FirstOrDefault(x => x.ID == parentID));
                }
                //cap nhat lai parentID
                parentID = list.FirstOrDefault(x => x.ID == parentID).ParentID;

            }
            return string.Join("->", list2.OrderBy(x => x.ParentID).Select(x => x.Name).ToArray());
        }

        public async Task<bool> IsExistsCode(int ID)
        {
            return await _context.OCs.AnyAsync(x => x.ID == ID);
        }

        public async Task<bool> Rename(TreeViewRenameOKR level)
        {
            var item = await _context.OKRs.FindAsync(level.key);
            item.Name = level.title;
            
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public async Task<IEnumerable<TreeViewOKR>> GetListTreeOC(int parentID, int id)
        {
            var listLevels = await _context.OKRs.OrderBy(x => x.Level).ToListAsync();
            var levels = new List<TreeViewOKR>();
            foreach (var item in listLevels)
            {
                var levelItem = new TreeViewOKR();
                levelItem.ID = item.ID;
                levelItem.Name = item.Name;
                levelItem.Level = item.Level;
                levelItem.ParentID = item.Parentid;
                levels.Add(levelItem);
            }

            List<TreeViewOKR> hierarchy = new List<TreeViewOKR>();

            hierarchy = levels.Where(c => c.ID == id && c.ParentID == parentID)
                            .Select(c => new TreeViewOKR()
                            {
                                ID = c.ID,
                                Name = c.Name,
                                Level = c.Level,
                                ParentID = c.ParentID,
                                children = GetTreeChildren(levels, c.ID)
                            })
                            .ToList();


            HieararchyWalkTree(hierarchy);

            return hierarchy;
        }

        private void HieararchyWalkTree(List<TreeViewOKR> hierarchy)
        {
            if (hierarchy != null)
            {
                foreach (var item in hierarchy)
                {
                    //Console.WriteLine(string.Format("{0} {1}", item.Id, item.Text));
                    HieararchyWalkTree(item.children);
                }
            }
        }
        public List<TreeViewOKR> GetTreeChildren(List<TreeViewOKR> levels, int parentid)
        {
            return levels
                    .Where(c => c.ParentID == parentid)
                    .Select(c => new TreeViewOKR()
                    {
                        ID = c.ID,
                        Name = c.Name,
                        Level = c.Level,
                        ParentID = c.ParentID,
                        children = GetTreeChildren(levels, c.ID)
                    })
                    .ToList();
        }

        public async Task<object> CreateOC(CreateOKRViewModel oc)
        {
            try
            {
                var item = _mapper.Map<Data.Models.OKR>(oc);
                item.Level = 1;

                await _context.OKRs.AddAsync(item);
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public async Task<object> CreateSubOC(CreateOKRViewModel oc)
        {
            var item = _mapper.Map<Data.Models.OC>(oc);

            //Level cha tang len 1 va gan parentid cho subtask
            var taskParent = _context.OKRs.Find(item.ParentID);
            item.Level = taskParent.Level + 1;
            item.ParentID = oc.Parentid;
            await _context.OCs.AddAsync(item);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int ID)
        {
            var item = await _context.OCs.FindAsync(ID);
            //var OCS = await _ocService.GetListTreeOC(item.ParentID, item.ID);
            //var arrOCs = GetAllDescendants(OCS).Select(x => x.ID).ToArray();
            //var items = await _context.Tasks.Where(x => arrOCs.Contains(x.ID)).ToListAsync();
            _context.OCs.Remove(item);
            try
            {
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
